﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class NT_NetworkDiscovery : NetworkDiscovery
{
    public TextMeshProUGUI StatusLabel;
    public TextMeshProUGUI AdressLabel;
    public TextMeshProUGUI DataLabel;

    public void Init(TextMeshProUGUI status, TextMeshProUGUI adress, TextMeshProUGUI data)
    {
        StatusLabel = status;
        AdressLabel = adress;
        DataLabel = data;
        AdressLabel.text = "";
        DataLabel.text = "";
        StartListening();
    }

    public void ChangeStatus()
    {
        if (isClient) StartBroadcast();
        else if (isServer) StartListening();
    }

    public void StartListening()
    {
        AdressLabel.text = "awaiting of broadcast...";
        DataLabel.text = "";
        if (running) StopBroadcast();
        Initialize();
        StartAsClient();
        StatusLabel.text = "running as Client";
        
    }

    public void StartBroadcast()
    {
        AdressLabel.text = "sending broadcast...";
        DataLabel.text = "";
        if (running) StopBroadcast();
        Initialize();
        StartAsServer();
        StatusLabel.text = "running as Server";
    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        base.OnReceivedBroadcast(fromAddress, data);

        AdressLabel.text = fromAddress;
        DataLabel.text = data;
    }

}
