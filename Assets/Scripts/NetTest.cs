﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class NetTest : MonoBehaviour
{
    public TextMeshProUGUI UNetLabel;
    public TextMeshProUGUI SocketsLabel;
    [Space (10)]
    [Header("Broadcast")]
    public NT_NetworkDiscovery Broadcast;
    public TextMeshProUGUI StatusLabel;
    public TextMeshProUGUI AdressLabel;
    public TextMeshProUGUI DataLabel;

    // Start is called before the first frame update
    void Start()
    {
        Broadcast.Init(StatusLabel, AdressLabel, DataLabel);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) Test();
    }

    public void Test()
    {
        UNetLabel.text = LocalIPAddress();
        SocketsLabel.text = LocalIPAddress();
    }

    public static string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "0.0.0.0";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }
}
